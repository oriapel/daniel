#include <Windows.h>
#define DLL_EXPORT
#include "mydll.h"
extern "C"
{
	DLL_EXPORT void	foo() {
		MessageBoxA(NULL, "really? 'LINKIN PARK????'", "-_-", NULL);
	}
}
	BOOL APIENTRY DllMain(HANDLE hModule, // Handle to DLL module
	DWORD ul_reason_for_call,
	LPVOID lpReserved) // Reserved
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		// A process is loading the DLL.
		foo();
		break;
	case DLL_THREAD_ATTACH:
		// A process is creating a new thread.
		break;
	case DLL_THREAD_DETACH:
		// A thread exits normally.
		break;
	case DLL_PROCESS_DETACH:
		// A process unloads the DLL.
		break;
	}
	return TRUE;
}