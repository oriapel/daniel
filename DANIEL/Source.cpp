#include <Windows.h>
#include <TlHelp32.h>
#include <string>

DWORD GetProcId(std::string);


int main()
{
	DWORD err;
	char dll[MAX_PATH];
	LPCSTR path = "C:\\Users\\magshimim\\source\\repos\\DANIEL\\x64\\Debug\\DANIEL.dll";
	// Get full path of DLL to inject
	DWORD pathLen = GetFullPathNameA(path, MAX_PATH, dll, NULL);
	// Get LoadLibrary function address �
	// the address doesn't change at remote process
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(GetModuleHandle("Kernel32.dll"), "LoadLibraryA" );
		// Open remote process
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, GetProcId("notepad.exe"));
	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
	PVOID memAddr = (PVOID)VirtualAllocEx(proc, 0, pathLen, MEM_COMMIT, PAGE_READWRITE);
	if (NULL == memAddr) {
		err = GetLastError();
		return 0;
	}
	// Write DLL name to remote process memory
	bool check = WriteProcessMemory(proc, memAddr, (LPVOID)path, pathLen, 0);
	if (0 == check) {
		err = GetLastError();
		return 0;
	}
	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	HANDLE hRemote = CreateRemoteThread(proc, 0, 0, (LPTHREAD_START_ROUTINE)addrLoadLibrary, memAddr, 0, 0);
	if (NULL == hRemote) {
		err = GetLastError();
		return 0;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	return 0;
}

//jsut something the i've found on google
DWORD GetProcId(std::string ProcName)
{
	PROCESSENTRY32   pe32;
	HANDLE         hSnapshot = NULL;

	pe32.dwSize = sizeof(PROCESSENTRY32);
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (Process32First(hSnapshot, &pe32))
	{
		do {
			if (strcmp(pe32.szExeFile, ProcName.c_str()) == 0)
				break;
		} while (Process32Next(hSnapshot, &pe32));
	}

	if (hSnapshot != INVALID_HANDLE_VALUE)
		CloseHandle(hSnapshot);

	return pe32.th32ProcessID;
}
